# ТРЕБОВАНИЯ ПЕРЕД ЗАПУСКОМ И УСТАНОВКОЙ:
* PYTHON >= 3.10 (сами)
* Poetry
```shell
pip install poetry
```

----------------

## Клоним репу
```shell
git clone git@gitlab.com:sergey_rubnikovich/tad_dvc.git
```


## Скачать картиночки по ссылке:
https://s3.amazonaws.com/fast-ai-imageclas/imagenette2-160.tgz


## Папки train, val из архива поместить в `data/row/`:
 * `data/row/train`
 * `data/row/val`

### Установить зависимости:
```shell
poetry install && poetry shell
```


### Создать где-нить папочку для dvc (путь_к_dvc_remote) и добавиьт в конфиге .dvc/config
```shell
dvc remote add -d remote_storage путь_к_dvc_remote
```


### Добавить папки с картинками в dvc:
```shell
dvc add data/row/train
dvc add data/row/val
dvc push
```

### Запуск пайплайна
```shell
dvc repro --force
```

#### Добавить новых картинок 
```shell
python src/augment.py
```

#### Обновление данных в соответствии с текущим комитом
```shell
dvc checkout
```

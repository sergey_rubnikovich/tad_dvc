import os
import pathlib
import random
from os.path import isfile, join, isdir

from PIL import Image

BASE_DIR = pathlib.Path(__file__).parent.parent
IMAGES_DIRNAME = 'data/raw/train'
IMAGES_DIR = BASE_DIR.joinpath(IMAGES_DIRNAME)
AUG_IMAGES_DIR = IMAGES_DIR


def get_random_rotate_angle(start=1, end=20) -> int:
    return random.randint(start, end)


def rotate_image(filepath, aug_filepath, rot_angle=5):
    with Image.open(filepath) as im:
        im_rotate = im.rotate(rot_angle)
        im_rotate.save(aug_filepath, quality=95)


def rotate_images(counter=100):
    for directory in os.listdir(IMAGES_DIR):
        path_to_directory = str(join(IMAGES_DIR, directory))
        c: int = 0
        if isdir(path_to_directory):
            for image in os.listdir(path_to_directory):
                if c == counter:
                    break

                path_to_image = join(path_to_directory, image)
                if isfile(path_to_image):
                    aug_filename = 'aug_' + str(image)
                    aug_filepath = join(path_to_directory, aug_filename)
                    rotate_image(join(path_to_directory, path_to_image), aug_filepath, get_random_rotate_angle())
                c += 1


def make_augmentation():
    rotate_images()


if __name__ == "__main__":
    make_augmentation()
